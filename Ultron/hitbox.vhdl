library IEEE;
use IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;
architecture behaviour of hitbox is

signal clk, reset, rip: std_logic;
signal x_red, x_blue, x00, x01, x10, x11: std_logic_vector(8 downto 0);
signal y_red, y_blue, y0, y1: std_logic_vector(7 downto 0);
signal height: std_logic_vector(5 downto 0);

component col_det_parent is
	port(clk   :in    std_logic;
        reset :in    std_logic;
        x_red :in    std_logic_vector(8 downto 0);
        x_blue:in    std_logic_vector(8 downto 0);
        y_red :in    std_logic_vector(7 downto 0);
        y_blue:in    std_logic_vector(7 downto 0);
        x00   :in    std_logic_vector(8 downto 0);
        x01   :in    std_logic_vector(8 downto 0);
        x10   :in    std_logic_vector(8 downto 0);
        x11   :in    std_logic_vector(8 downto 0);
        height:in    std_logic_vector(5 downto 0);
        y0    :in    std_logic_vector(7 downto 0);
        y1    :in    std_logic_vector(7 downto 0);
        rip   :out   std_logic
); 
end component;

begin
	lbl1: col_det_parent port map(clk, reset, x_red, x_blue, y_red, y_blue, x00, x01, x10, x11, height, y0, y1, rip);
		clk    <= '1' after 0 ns, '0' after 1 ns when clk /= '0' else '1' after 1 ns;
		reset	 <= '1', '0' after 3 ns;
		-- x_red	 <= "001010100" after 0 ns;	-- 21
		-- x_blue <= "011101100" after 0 ns;	-- 59
		-- y_red	 <= "10000110" after 0 ns;	-- 134
		-- y_blue <= "01000110" after 0 ns;		-- 134
		-- x00    <= "000000000" after 0 ns;	-- 0
		-- x01    <= "100110100" after 0 ns;	-- 77
		-- x10    <= "000110100" after 0 ns;	-- 13
		-- x11    <= "111001100" after 0 ns;	-- 115
		x_red  <= "001011100" after 0 ns;	-- 23
		x_blue <= "011100100" after 0 ns;	-- 57
		y_red  <= "10001101" after 0 ns;	-- 141
		y_blue <= "01111111" after 0 ns;	-- 127
		x00    <= "000000000" after 0 ns;	-- 0
		x01    <= "010110100" after 0 ns;	-- 45
		x10    <= "010001100" after 0 ns;	-- 35
		x11    <= "101000000" after 0 ns;	-- 80
		
		height <= "001000" after 0 ns;		-- 8
		
		y0     <= "10000111" after 0 ns,	-- 134
					"10010001" after 120 ns,-- 131
					"01111100" after 240 ns;-- 124
		y1     <= "10110100" after 0 ns, "10010001" after 120 ns, "10000111" after 240 ns,  "01000111" after 800 ns;
		
end behaviour;