
library ieee,CellsLib;

use ieee.std_logic_1164.all;
use CellsLib.CellsLib_DECL_PACK.all;

architecture synthesised of typebuilder is

   component iv110
      port( A : in std_logic;  Y : out std_logic);
   end component;
   
   component ex210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component dfa11
      port( D, CK, R : in std_logic;  Q : out std_logic);
   end component;
   
   signal blocktype_2_port, blocktype_1_port, blocktype_0_port, count_1_port, 
      count_0_port, n2, n3 : std_logic;

begin
   blocktype <= ( blocktype_2_port, blocktype_1_port, blocktype_0_port );
   
   count_reg_0_inst : dfa11 port map( D => n2, CK => clk, R => reset, Q => 
                           count_0_port);
   count_reg_1_inst : dfa11 port map( D => count_0_port, CK => clk, R => reset,
                           Q => count_1_port);
   count_reg_2_inst : dfa11 port map( D => count_1_port, CK => clk, R => reset,
                           Q => blocktype_0_port);
   count_reg_3_inst : dfa11 port map( D => blocktype_0_port, CK => clk, R => 
                           reset, Q => blocktype_1_port);
   count_reg_5_inst : dfa11 port map( D => blocktype_1_port, CK => clk, R => 
                           reset, Q => blocktype_2_port);
   U5 : ex210 port map( A => n3, B => count_0_port, Y => n2);
   U6 : iv110 port map( A => blocktype_2_port, Y => n3);

end synthesised;



