library IEEE;
use IEEE.std_logic_1164.ALL;

architecture behaviour of ultron_tb is

component ultron is
   port(clk   :in    std_logic;
        reset :in    std_logic;
        left  :in    std_logic;
        right :in    std_logic;
        x_red :out   std_logic_vector(6 downto 0);
        y_red :out   std_logic_vector(7 downto 0);
        x_blue:out   std_logic_vector(6 downto 0);
        y_blue:out   std_logic_vector(7 downto 0));
end component;

signal clk: std_logic;
signal reset: std_logic;
signal left: std_logic;
signal right: std_logic;
signal x_red, x_blue: std_logic_vector(6 downto 0);
signal y_red, y_blue: std_logic_vector(7 downto 0);

begin

lbl1: ultron port map(clk, reset, left, right, x_red, y_red, x_blue, y_blue);
		clk <= '1' after 0 ns, '0' after 1 ns when clk /= '0' else '1' after 1 ns;
		reset <= '1', '0' after 3 ns;
		left <= '0', '1' after 20 ms, '0' after 3200 ms; --, '1' after 1300 ns, '0' after 5300 ns;
		right <= '0', '1' after 3250 ms, '0' after 5900 ms; --, '1' after 5500 ns, '0' after 8000 ns;

end behaviour;


