library IEEE;
use IEEE.std_logic_1164.ALL;

entity coor_calc is
   port(y_o1 :in    std_logic_vector(7 downto 0);
        y_o2 :in    std_logic_vector(7 downto 0);
        y_o3 :in    std_logic_vector(7 downto 0);
        y_o4 :in    std_logic_vector(7 downto 0);
        y_b1 :in    std_logic_vector(7 downto 0);
        y_b2 :in    std_logic_vector(7 downto 0);
        x_b1 :in    std_logic_vector(6 downto 0);
        x_b2 :in    std_logic_vector(6 downto 0);
        y2_o1:out   std_logic_vector(7 downto 0);
        y2_o2:out   std_logic_vector(7 downto 0);
        y2_o3:out   std_logic_vector(7 downto 0);
        y2_o4:out   std_logic_vector(7 downto 0);
        y2_b1:out   std_logic_vector(7 downto 0);
        y2_b2:out   std_logic_vector(7 downto 0);
        x2_b1:out   std_logic_vector(6 downto 0);
        x2_b2:out   std_logic_vector(6 downto 0));
end coor_calc;

