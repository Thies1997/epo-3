library IEEE;
use IEEE.std_logic_1164.ALL;


architecture structural of bar_control is
  
  component typebuilder
    port (
      clk         :in  std_logic;                                     
      reset       :in  std_logic;
      blocktype   :out std_logic_vector (2 downto 0)
    );
  end component;

  component counter
    port (clk       : in std_logic;
          reset     : in std_logic;
          count_out : out std_logic
  );
  end component;

  component bar
	port( counter      :in     std_logic;
		    reset        :in     std_logic;
		    blocktype    :in     std_logic_vector(2 downto 0);
        output_y0    :out    unsigned(7 downto 0);
        output_y1    :out    unsigned(7 downto 0);
        output_y2    :out    unsigned(7 downto 0);
        output_y3    :out    unsigned(7 downto 0);
        output_type0 :out    std_logic_vector(2 downto 0);
        output_type1 :out    std_logic_vector(2 downto 0);
        output_type2 :out    std_logic_vector(2 downto 0);
        output_type3 :out    std_logic_vector(2 downto 0)
	);
  end component;
  signal cnt: std_logic;
  signal btype: std_logic_vector (2 downto 0);

  begin
    
  C1: typebuilder port map (
            cnt, 
            reset,
            btype);
            
  C2: counter port map (
            clk,
            reset,
            cnt);
            
  C3: bar port map (
            cnt,
		        reset,
		        btype,
            output_y0,
            output_y1,
            output_y2,
            output_y3,
            output_type0,
            output_type1,
            output_type2,
            output_type3);
            
  end architecture structural;

