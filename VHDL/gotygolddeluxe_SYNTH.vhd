
library ieee,CellsLib;

use ieee.std_logic_1164.all;
use CellsLib.CellsLib_DECL_PACK.all;

architecture synthesised of gotygolddeluxe is

   component bar
      port( counter, reset : in std_logic;  blocktype : in std_logic_vector (2 
            downto 0);  output_y0, output_y1 : inout std_logic_vector (7 downto
            0);  output_y2, output_y3 : out std_logic_vector (7 downto 0);  
            output_type0, output_type1 : inout std_logic_vector (2 downto 0);  
            output_type2, output_type3 : out std_logic_vector (2 downto 0));
   end component;
   
   component counter
      port( clk, reset, rip : in std_logic;  count_out : out std_logic);
   end component;
   
   component typebuilder
      port( clk, reset : in std_logic;  blocktype : out std_logic_vector (2 
            downto 0));
   end component;
   
   component coordcalc
      port( clk, reset : in std_logic;  quadrant : in std_logic_vector (1 
            downto 0);  step : in std_logic_vector (3 downto 0);  x_red : out 
            std_logic_vector (6 downto 0);  y_red : out std_logic_vector (7 
            downto 0);  x_blue : out std_logic_vector (6 downto 0);  y_blue : 
            out std_logic_vector (7 downto 0));
   end component;
   
   component controller
      port( counter_clk, reset, left, right : in std_logic;  quadrant : inout 
            std_logic_vector (1 downto 0);  step : inout std_logic_vector (3 
            downto 0));
   end component;
   
   component decoder
      port( clk, reset : in std_logic;  block_type : in std_logic_vector (2 
            downto 0);  x0, x1 : out std_logic_vector (6 downto 0);  height : 
            out std_logic_vector (5 downto 0));
   end component;
   
   component subtraction
      port( clk, reset : in std_logic;  value1, value2 : in std_logic_vector (7
            downto 0);  result : out std_logic_vector (7 downto 0));
   end component;
   
   component hit_math_unit
      port( clk, reset, start : in std_logic;  x_circle : in std_logic_vector 
            (6 downto 0);  y_circle : in std_logic_vector (7 downto 0);  
            x_block_0, x_block_1 : in std_logic_vector (6 downto 0);  y_block :
            in std_logic_vector (7 downto 0);  height : in std_logic_vector (5 
            downto 0);  value1, value2 : out std_logic_vector (7 downto 0);  
            result : in std_logic_vector (7 downto 0);  started, done, rip : 
            out std_logic);
   end component;
   
   component hitman2
      port( clk, reset : in std_logic;  x_red, x_blue : in std_logic_vector (6 
            downto 0);  y_red, y_blue : in std_logic_vector (7 downto 0);  
            type0, type1 : inout std_logic_vector (2 downto 0);  height : in 
            std_logic_vector (5 downto 0);  y0, y1 : inout std_logic_vector (7 
            downto 0);  done, started : in std_logic;  start : out std_logic;  
            block_type : out std_logic_vector (2 downto 0);  x_circle : out 
            std_logic_vector (6 downto 0);  y_circle, y_block : out 
            std_logic_vector (7 downto 0));
   end component;
   
   signal X_Logic0_port, x_red_6_port, x_red_5_port, x_red_4_port, x_red_3_port
      , x_red_2_port, x_red_1_port, x_red_0_port, x_blue_6_port, x_blue_5_port,
      x_blue_4_port, x_blue_3_port, x_blue_2_port, x_blue_1_port, x_blue_0_port
      , y_red_7_port, y_red_6_port, y_red_5_port, y_red_4_port, y_red_3_port, 
      y_red_2_port, y_red_1_port, y_red_0_port, y_blue_7_port, y_blue_6_port, 
      y_blue_5_port, y_blue_4_port, y_blue_3_port, y_blue_2_port, y_blue_1_port
      , y_blue_0_port, height_5_port, height_4_port, height_3_port, 
      height_2_port, height_1_port, height_0_port, done, started, start, 
      hit_block_type_2_port, hit_block_type_1_port, hit_block_type_0_port, 
      x_circle_6_port, x_circle_5_port, x_circle_4_port, x_circle_3_port, 
      x_circle_2_port, x_circle_1_port, x_circle_0_port, y_circle_7_port, 
      y_circle_6_port, y_circle_5_port, y_circle_4_port, y_circle_3_port, 
      y_circle_2_port, y_circle_1_port, y_circle_0_port, y_block_7_port, 
      y_block_6_port, y_block_5_port, y_block_4_port, y_block_3_port, 
      y_block_2_port, y_block_1_port, y_block_0_port, x_block_0_6_port, 
      x_block_0_5_port, x_block_0_4_port, x_block_0_3_port, x_block_0_2_port, 
      x_block_0_1_port, x_block_0_0_port, x_block_1_6_port, x_block_1_5_port, 
      x_block_1_4_port, x_block_1_3_port, x_block_1_2_port, x_block_1_1_port, 
      x_block_1_0_port, value1_7_port, value1_6_port, value1_5_port, 
      value1_4_port, value1_3_port, value1_2_port, value1_1_port, value1_0_port
      , value2_7_port, value2_6_port, value2_5_port, value2_4_port, 
      value2_3_port, value2_2_port, value2_1_port, value2_0_port, result_7_port
      , result_6_port, result_5_port, result_4_port, result_3_port, 
      result_2_port, result_1_port, result_0_port, quadrant_1_port, 
      quadrant_0_port, step_3_port, step_2_port, step_1_port, step_0_port, 
      btype_2_port, btype_1_port, btype_0_port, n_1000, n_1001, n_1002, n_1003,
      n_1004, n_1005, n_1006, n_1007, n_1008, n_1009, n_1010, n_1011, n_1012, 
      n_1013, n_1014, n_1015, n_1016 : std_logic;

begin
   
   hit_states : hitman2 port map( clk => clk, reset => reset, x_red(6) => 
                           x_red_6_port, x_red(5) => x_red_5_port, x_red(4) => 
                           x_red_4_port, x_red(3) => x_red_3_port, x_red(2) => 
                           x_red_2_port, x_red(1) => x_red_1_port, x_red(0) => 
                           x_red_0_port, x_blue(6) => x_blue_6_port, x_blue(5) 
                           => x_blue_5_port, x_blue(4) => x_blue_4_port, 
                           x_blue(3) => x_blue_3_port, x_blue(2) => 
                           x_blue_2_port, x_blue(1) => x_blue_1_port, x_blue(0)
                           => x_blue_0_port, y_red(7) => y_red_7_port, y_red(6)
                           => y_red_6_port, y_red(5) => y_red_5_port, y_red(4) 
                           => y_red_4_port, y_red(3) => y_red_3_port, y_red(2) 
                           => y_red_2_port, y_red(1) => y_red_1_port, y_red(0) 
                           => y_red_0_port, y_blue(7) => y_blue_7_port, 
                           y_blue(6) => y_blue_6_port, y_blue(5) => 
                           y_blue_5_port, y_blue(4) => y_blue_4_port, y_blue(3)
                           => y_blue_3_port, y_blue(2) => y_blue_2_port, 
                           y_blue(1) => y_blue_1_port, y_blue(0) => 
                           y_blue_0_port, type0(2) => output_type0(2), type0(1)
                           => output_type0(1), type0(0) => output_type0(0), 
                           type1(2) => output_type1(2), type1(1) => 
                           output_type1(1), type1(0) => output_type1(0), 
                           height(5) => height_5_port, height(4) => 
                           height_4_port, height(3) => height_3_port, height(2)
                           => height_2_port, height(1) => height_1_port, 
                           height(0) => height_0_port, y0(7) => n_1000, y0(6) 
                           => n_1001, y0(5) => n_1002, y0(4) => n_1003, y0(3) 
                           => n_1004, y0(2) => n_1005, y0(1) => n_1006, y0(0) 
                           => n_1007, y1(7) => n_1008, y1(6) => n_1009, y1(5) 
                           => n_1010, y1(4) => n_1011, y1(3) => n_1012, y1(2) 
                           => n_1013, y1(1) => n_1014, y1(0) => n_1015, done =>
                           done, started => started, start => start, 
                           block_type(2) => hit_block_type_2_port, 
                           block_type(1) => hit_block_type_1_port, 
                           block_type(0) => hit_block_type_0_port, x_circle(6) 
                           => x_circle_6_port, x_circle(5) => x_circle_5_port, 
                           x_circle(4) => x_circle_4_port, x_circle(3) => 
                           x_circle_3_port, x_circle(2) => x_circle_2_port, 
                           x_circle(1) => x_circle_1_port, x_circle(0) => 
                           x_circle_0_port, y_circle(7) => y_circle_7_port, 
                           y_circle(6) => y_circle_6_port, y_circle(5) => 
                           y_circle_5_port, y_circle(4) => y_circle_4_port, 
                           y_circle(3) => y_circle_3_port, y_circle(2) => 
                           y_circle_2_port, y_circle(1) => y_circle_1_port, 
                           y_circle(0) => y_circle_0_port, y_block(7) => 
                           y_block_7_port, y_block(6) => y_block_6_port, 
                           y_block(5) => y_block_5_port, y_block(4) => 
                           y_block_4_port, y_block(3) => y_block_3_port, 
                           y_block(2) => y_block_2_port, y_block(1) => 
                           y_block_1_port, y_block(0) => y_block_0_port);
   math_unit : hit_math_unit port map( clk => clk, reset => reset, start => 
                           start, x_circle(6) => x_circle_6_port, x_circle(5) 
                           => x_circle_5_port, x_circle(4) => x_circle_4_port, 
                           x_circle(3) => x_circle_3_port, x_circle(2) => 
                           x_circle_2_port, x_circle(1) => x_circle_1_port, 
                           x_circle(0) => x_circle_0_port, y_circle(7) => 
                           y_circle_7_port, y_circle(6) => y_circle_6_port, 
                           y_circle(5) => y_circle_5_port, y_circle(4) => 
                           y_circle_4_port, y_circle(3) => y_circle_3_port, 
                           y_circle(2) => y_circle_2_port, y_circle(1) => 
                           y_circle_1_port, y_circle(0) => y_circle_0_port, 
                           x_block_0(6) => x_block_0_6_port, x_block_0(5) => 
                           x_block_0_5_port, x_block_0(4) => x_block_0_4_port, 
                           x_block_0(3) => x_block_0_3_port, x_block_0(2) => 
                           x_block_0_2_port, x_block_0(1) => x_block_0_1_port, 
                           x_block_0(0) => x_block_0_0_port, x_block_1(6) => 
                           x_block_1_6_port, x_block_1(5) => x_block_1_5_port, 
                           x_block_1(4) => x_block_1_4_port, x_block_1(3) => 
                           x_block_1_3_port, x_block_1(2) => x_block_1_2_port, 
                           x_block_1(1) => x_block_1_1_port, x_block_1(0) => 
                           x_block_1_0_port, y_block(7) => y_block_7_port, 
                           y_block(6) => y_block_6_port, y_block(5) => 
                           y_block_5_port, y_block(4) => y_block_4_port, 
                           y_block(3) => y_block_3_port, y_block(2) => 
                           y_block_2_port, y_block(1) => y_block_1_port, 
                           y_block(0) => y_block_0_port, height(5) => 
                           height_5_port, height(4) => height_4_port, height(3)
                           => height_3_port, height(2) => height_2_port, 
                           height(1) => height_1_port, height(0) => 
                           height_0_port, value1(7) => value1_7_port, value1(6)
                           => value1_6_port, value1(5) => value1_5_port, 
                           value1(4) => value1_4_port, value1(3) => 
                           value1_3_port, value1(2) => value1_2_port, value1(1)
                           => value1_1_port, value1(0) => value1_0_port, 
                           value2(7) => value2_7_port, value2(6) => 
                           value2_6_port, value2(5) => value2_5_port, value2(4)
                           => value2_4_port, value2(3) => value2_3_port, 
                           value2(2) => value2_2_port, value2(1) => 
                           value2_1_port, value2(0) => value2_0_port, result(7)
                           => result_7_port, result(6) => result_6_port, 
                           result(5) => result_5_port, result(4) => 
                           result_4_port, result(3) => result_3_port, result(2)
                           => result_2_port, result(1) => result_1_port, 
                           result(0) => result_0_port, started => started, done
                           => done, rip => rip);
   subtractor : subtraction port map( clk => clk, reset => reset, value1(7) => 
                           value1_7_port, value1(6) => value1_6_port, value1(5)
                           => value1_5_port, value1(4) => value1_4_port, 
                           value1(3) => value1_3_port, value1(2) => 
                           value1_2_port, value1(1) => value1_1_port, value1(0)
                           => value1_0_port, value2(7) => value2_7_port, 
                           value2(6) => value2_6_port, value2(5) => 
                           value2_5_port, value2(4) => value2_4_port, value2(3)
                           => value2_3_port, value2(2) => value2_2_port, 
                           value2(1) => value2_1_port, value2(0) => 
                           value2_0_port, result(7) => result_7_port, result(6)
                           => result_6_port, result(5) => result_5_port, 
                           result(4) => result_4_port, result(3) => 
                           result_3_port, result(2) => result_2_port, result(1)
                           => result_1_port, result(0) => result_0_port);
   type_decoder : decoder port map( clk => clk, reset => reset, block_type(2) 
                           => hit_block_type_2_port, block_type(1) => 
                           hit_block_type_1_port, block_type(0) => 
                           hit_block_type_0_port, x0(6) => x_block_0_6_port, 
                           x0(5) => x_block_0_5_port, x0(4) => x_block_0_4_port
                           , x0(3) => x_block_0_3_port, x0(2) => 
                           x_block_0_2_port, x0(1) => x_block_0_1_port, x0(0) 
                           => x_block_0_0_port, x1(6) => x_block_1_6_port, 
                           x1(5) => x_block_1_5_port, x1(4) => x_block_1_4_port
                           , x1(3) => x_block_1_3_port, x1(2) => 
                           x_block_1_2_port, x1(1) => x_block_1_1_port, x1(0) 
                           => x_block_1_0_port, height(5) => height_5_port, 
                           height(4) => height_4_port, height(3) => 
                           height_3_port, height(2) => height_2_port, height(1)
                           => height_1_port, height(0) => height_0_port);
   input_controller : controller port map( counter_clk => X_Logic0_port, reset 
                           => reset, left => left, right => right, quadrant(1) 
                           => quadrant_1_port, quadrant(0) => quadrant_0_port, 
                           step(3) => step_3_port, step(2) => step_2_port, 
                           step(1) => step_1_port, step(0) => step_0_port);
   coordinate_generator : coordcalc port map( clk => clk, reset => reset, 
                           quadrant(1) => quadrant_1_port, quadrant(0) => 
                           quadrant_0_port, step(3) => step_3_port, step(2) => 
                           step_2_port, step(1) => step_1_port, step(0) => 
                           step_0_port, x_red(6) => x_red_6_port, x_red(5) => 
                           x_red_5_port, x_red(4) => x_red_4_port, x_red(3) => 
                           x_red_3_port, x_red(2) => x_red_2_port, x_red(1) => 
                           x_red_1_port, x_red(0) => x_red_0_port, y_red(7) => 
                           y_red_7_port, y_red(6) => y_red_6_port, y_red(5) => 
                           y_red_5_port, y_red(4) => y_red_4_port, y_red(3) => 
                           y_red_3_port, y_red(2) => y_red_2_port, y_red(1) => 
                           y_red_1_port, y_red(0) => y_red_0_port, x_blue(6) =>
                           x_blue_6_port, x_blue(5) => x_blue_5_port, x_blue(4)
                           => x_blue_4_port, x_blue(3) => x_blue_3_port, 
                           x_blue(2) => x_blue_2_port, x_blue(1) => 
                           x_blue_1_port, x_blue(0) => x_blue_0_port, y_blue(7)
                           => y_blue_7_port, y_blue(6) => y_blue_6_port, 
                           y_blue(5) => y_blue_5_port, y_blue(4) => 
                           y_blue_4_port, y_blue(3) => y_blue_3_port, y_blue(2)
                           => y_blue_2_port, y_blue(1) => y_blue_1_port, 
                           y_blue(0) => y_blue_0_port);
   type_generator : typebuilder port map( clk => X_Logic0_port, reset => reset,
                           blocktype(2) => btype_2_port, blocktype(1) => 
                           btype_1_port, blocktype(0) => btype_0_port);
   clock60 : counter port map( clk => clk, reset => reset, rip => X_Logic0_port
                           , count_out => n_1016);
   bar_movement : bar port map( counter => X_Logic0_port, reset => reset, 
                           blocktype(2) => btype_2_port, blocktype(1) => 
                           btype_1_port, blocktype(0) => btype_0_port, 
                           output_y0(7) => output_y0(7), output_y0(6) => 
                           output_y0(6), output_y0(5) => output_y0(5), 
                           output_y0(4) => output_y0(4), output_y0(3) => 
                           output_y0(3), output_y0(2) => output_y0(2), 
                           output_y0(1) => output_y0(1), output_y0(0) => 
                           output_y0(0), output_y1(7) => output_y1(7), 
                           output_y1(6) => output_y1(6), output_y1(5) => 
                           output_y1(5), output_y1(4) => output_y1(4), 
                           output_y1(3) => output_y1(3), output_y1(2) => 
                           output_y1(2), output_y1(1) => output_y1(1), 
                           output_y1(0) => output_y1(0), output_y2(7) => 
                           output_y2(7), output_y2(6) => output_y2(6), 
                           output_y2(5) => output_y2(5), output_y2(4) => 
                           output_y2(4), output_y2(3) => output_y2(3), 
                           output_y2(2) => output_y2(2), output_y2(1) => 
                           output_y2(1), output_y2(0) => output_y2(0), 
                           output_y3(7) => output_y3(7), output_y3(6) => 
                           output_y3(6), output_y3(5) => output_y3(5), 
                           output_y3(4) => output_y3(4), output_y3(3) => 
                           output_y3(3), output_y3(2) => output_y3(2), 
                           output_y3(1) => output_y3(1), output_y3(0) => 
                           output_y3(0), output_type0(2) => output_type0(2), 
                           output_type0(1) => output_type0(1), output_type0(0) 
                           => output_type0(0), output_type1(2) => 
                           output_type1(2), output_type1(1) => output_type1(1),
                           output_type1(0) => output_type1(0), output_type2(2) 
                           => output_type2(2), output_type2(1) => 
                           output_type2(1), output_type2(0) => output_type2(0),
                           output_type3(2) => output_type3(2), output_type3(1) 
                           => output_type3(1), output_type3(0) => 
                           output_type3(0));
   X_Logic0_port <= '0';

end synthesised;



