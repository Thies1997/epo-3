library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

architecture behaviour of vga_startscherm is

	signal con_col : std_logic_vector(7 downto 0);
	signal con_row : std_logic_vector (6 downto 0);

begin
VIDEO_DISPLAY_DATA : process (clk)

begin
	if clk'event and clk = '1' then
		rom_mux_output <= rom_data ((CONV_INTEGER(con_row(2 downto 0))));

		if row_address >= "0101010" and row_address < "0110010" and col_address > "0100011" and col_address <= "0101010" then  -- T
			con_col <= col_address - "0100011"; 
			con_row <= row_address - "0101010"; 
			rom_address <= ("110" & con_col(2 downto 0));
			

		elsif row_address >= "0110100" and row_address < "0111100" and col_address > "0100011" and col_address <= "0101010" then -- E
			con_col <= col_address - "0100011";
			con_row <= row_address - "0110100";
			rom_address <= ("010" & con_col(2 downto 0));
			
		elsif row_address >= "0111110" and row_address < "1000110" and col_address > "0100011" and col_address <= "0101010" then -- U
			con_col <= col_address - "0100011";
			con_row <= row_address - "0111110";
			rom_address <= ("101" & con_col(2 downto 0));
			
		elsif row_address >= "1001000" and row_address < "1010000" and col_address > "0100011" and col_address <= "0101010" then -- D
			con_col <= col_address - "0100011";
			con_row <= row_address - "1001000";
			rom_address <= ("100" & con_col(2 downto 0));
			
		--------------------------------------------------------------------------------------------------------------------------------------------
			
		elsif row_address >= "0100111" and row_address < "0101111" and col_address > "0110111" and col_address <= "0111110" then  -- S
			con_col <= col_address - "0110111"; 
			con_row <= row_address - "0100111"; 
			rom_address <= ("011" & con_col(2 downto 0));
			

		elsif row_address >= "0110000" and row_address < "0111000" and col_address > "0110111" and col_address <= "0111110" then -- S
			con_col <= col_address - "0110111";
			con_row <= row_address - "0110000";
			rom_address <= ("011" & con_col(2 downto 0));
			
		elsif row_address >= "0111001" and row_address < "1000001" and col_address > "0110111" and col_address <= "0111110" then -- E
			con_col <= col_address - "0110111";
			con_row <= row_address - "0111001";
			rom_address <= ("010" & con_col(2 downto 0));
			
		elsif row_address >= "1000010" and row_address < "1001010" and col_address > "0110111" and col_address <= "0111110" then -- R
			con_col <= col_address - "0110111";
			con_row <= row_address - "1000010";
			rom_address <= ("001" & con_col(2 downto 0));
			
		elsif row_address >= "1001011" and row_address < "1010011" and col_address > "0110111" and col_address <= "0111110" then  -- P
			con_col <= col_address - "0110111"; 
			con_row <= row_address - "1001011"; 
			rom_address <= ("000" & con_col(2 downto 0));
		
		--------------------------------------------------------------------------------------------------------------------------------------------

		elsif row_address >= "0100111" and row_address < "0101111" and col_address > "1000000" and col_address <= "1000111" then -- T
			con_col <= col_address - "1000000";
			con_row <= row_address - "0100111";
			rom_address <= ("110" & con_col(2 downto 0));
			
		elsif row_address >= "0110000" and row_address < "0111000" and col_address > "1000000" and col_address <= "1000111" then -- R
			con_col <= col_address - "1000000";
			con_row <= row_address - "0110000";
			rom_address <= ("001" & con_col(2 downto 0));
			
		elsif row_address >= "0111001" and row_address < "1000001" and col_address > "1000000" and col_address <= "1000111" then -- A
			con_col <= col_address - "1000000";
			con_row <= row_address - "0111001";
			rom_address <= ("111" & con_col(2 downto 0));
			
		elsif row_address >= "1000010" and row_address < "1001010" and col_address > "1000000" and col_address <= "1000111" then  -- T
			con_col <= col_address - "1000000"; 
			con_row <= row_address - "1000010"; 
			rom_address <= ("110" & con_col(2 downto 0));

		elsif row_address >= "1001011" and row_address < "1010011" and col_address > "1000000" and col_address <= "1000111" then -- S
			con_col <= col_address - "1000000";
			con_row <= row_address - "1001011";
			rom_address <= ("011" & con_col(2 downto 0));
			
		else
			con_col <= "00000000";
			con_row <= "0000000";
			rom_address <= "001000";	-- 8
			
		end if;

	end if;
end process VIDEO_DISPLAY_DATA;

end behaviour;
