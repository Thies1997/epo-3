library IEEE;
use IEEE.std_logic_1164.ALL;

entity controller is
	port(clk    :in    std_logic;
			counter_clk		:in    std_logic;
			reset	     :in    std_logic;
			left       :in    std_logic;
			right      :in    std_logic;
			quadrant   :out   std_logic_vector(1 downto 0);
			step       :out   std_logic_vector(3 downto 0);
			enable     :in		std_logic;
			enable_reset:in std_logic
	);
end controller;




















