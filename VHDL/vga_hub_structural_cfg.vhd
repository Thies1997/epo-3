configuration vga_hub_structural_cfg of vga_hub is
   for structural
      for all: coor_calc use configuration work.coor_calc_behaviour_cfg;
      end for;
      for all: video_display use configuration work.video_display_behaviour_cfg;
      end for;
      for all: rom use configuration work.rom_behavioral_cfg;
      end for;
      for all: vga_core use configuration work.vga_core_behaviour_cfg;
      end for;
      for all: vga_output use configuration work.vga_output_behaviour_cfg;
      end for;
   end for;
end vga_hub_structural_cfg;


