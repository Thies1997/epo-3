library IEEE;
use IEEE.std_logic_1164.ALL;

entity vga_sync is
port( clk, reset : in std_logic;
      hsync, vsync : out std_logic;
      row_address : out std_logic_vector(6 downto 0);
      col_address : out std_logic_vector(7 downto 0));
end vga_sync;

