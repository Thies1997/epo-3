library IEEE;
use IEEE.std_logic_1164.ALL;

architecture behaviour of counter is
  
signal count, new_count : unsigned (16 downto 0);
  
begin

count_out <= std_logic (count(16));

P1: process (clk, reset, count, new_count)
begin
	if (rising_edge (clk)) then
		if (reset = '1' OR (count(16)='1' AND count(15)='1' AND count(12)='1')) then
			count <= "00000000000000000";
		else
			count <= new_count;
			new_count <= count + 1;
		end if;
	end if;
end process;

end behaviour;
